## @file ClosedLoop.py
#  This script is a module that runs a closed loop controller.
#
#  @package ClosedLoop
#  This module contains the closed loop controller related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 25, 2020


import utime

## @brief The ClosedLoop class
class CL:
    ## Proportional controller
    
    ## @brief
    def __init__(self,interval):
        
        
        ## Proportional gain
        self.Kp = 0 
        
        ## Reference omega used in the proportional controller
        self.omegaRef = 0
        
        ## Actual omega used in the proportional controller
        self.omega = 0
        
        #self.level = 0
        
        ## Delta level output of the proportional controller equation
        self.dellevel = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)        
        
    ## Runs one iteration
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            # runtime code                    
            
            # Proportional controller equation
            self.dellevel = float(self.Kp*(self.omegaRef - self.omega))
            
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp        
            
    ## @brief Set the proportional gain constant
    def setKp(self,kp):
        
        self.Kp = float(kp)
        
    ## @brief Return the proportional gain constant
    def getKp(self):
        
        return self.Kp
    
    ## @brief Set the reference omega value
    def setOmegaRef(self, omref):
        
        self.omegaRef = float(omref)
        
    ## @brief Return the reference omega value
    def getOmegaRef(self):
        
        return self.omegaRef
        
    ## @brief Set the measured/real omega value
    def setOmega(self, om):
        
        self.omega = float(om)
        
    ## @brief disable the controller by setting Kp to zero. Output of the controller should fall to zero and stop the process.
    def disable(self):
        
        self.Kp = 0
    
    ## @brief Return the output of the proportional controller
    def getLevel(self):
        
        return self.dellevel
