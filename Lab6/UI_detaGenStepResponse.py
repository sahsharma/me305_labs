## @file UI_detaGenStepResponse.py
#  This script is a module that contains Encoder User Interface classes and methods to be used in other scripts.
#  It interfaces between the motor driver, encoder, closed loop controller, and frontend to produce a step response using user-input values for proportional gain.
#  This script is built to generate a step response profile in motor 1 of the ME305/405 balancing board. The step reference fires after 1 second up to 1200 RPM.
#
#  @package UI_detaGenStepResponse
#  This module contains the EncoderData interface related classes and methods specific to Lab 6
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 25, 2020



from pyb import UART
import utime
import array


#============================================================================#
#============================================================================#

## @brief   An EncoderData class
class EncoderDataLab6:
    ## @details   associates an encoder to send and recive data through UART
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0
    
    ## Constant defining State 1
    S1_WAIT     = 1
    
    ## Constant defining State 2
    S2_COLLECT  = 2
    
    ## Constant defining State 3
    S3_STOP     = 3
    
    
    ## @brief    Creates the EncoderData object
    def __init__(self, myuart, ENC, CL, MD, datapoints, sampleRate, interval):
        
        
        ## @brief   myuart object
        self.myuart = UART(myuart)
        
        ## @brief   ENC object
        self.ENC = ENC # Encoder object to steal data from
        
        ## Closed loop object to act on
        self.CL = CL
        
        ## Motor Driver object to control
        self.MD = MD        
        
        ## omega reference value to send to the closed loop controller
        self.omref = 0
        
        ## variable to save the output of the closed loop controller
        self.level = 0
        
        ## @brief   sampleRate object
        self.sampleRate = int((1/sampleRate)*1e6)  #in Hz then converted to us
                
        ## @brief   datapoints object
        self.datapoints = datapoints # total number od data points to gather for a full run
        
        ## @brief   Array to hold encoder data
        self.arrayData = 0
        
        ## @brief   Array to hold time data
        self.arrayTime = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## @brief   The current time that will be updated as the code runs
        self.curr_time = 0
        
        ## @brief   Logs the number of samples that have been cycled during data collection
        self.samples = 0

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT 

        ## @brief   val object
        self.val = ''
        
        ## intermediate variable
        self.fullval = ''
        
        ## intermediate variable
        self.valref = 0
        
        ## @brief   The statement that will be printed when data collection stops
        self.terminationStatement = 0
        
        ## @brief   Buffer value that will be added to self.arrayData
        self.bufferVal = 0
        
        ## Buffer value that appends itself to the reference array
        self.bufferValref = 0
        
        ## @brief   Buffer time that will be added tot self.arrayTime
        self.bufferTime = 0
        
        ## @brief   Logs the length of the data arrays. Similar to self.samples
        self.arrayLengthCounter = 0
        
        ## @brief   The timestamp for when data collection begins
        self.begin_time = 0
        
        ## @brief   The timestamp for when to collect the next datt point/sample
        self.next_timeSample = 0

    ## @brief   runs one iteration of the task
    def run(self):        
    
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp        
        
        # State 0 Init
        if (self.state == self.S0_INIT):       
            
            self.transitionTo(self.S1_WAIT)

            self.arrayData = array.array('f',[])
            
            ## Array that holds the reference omega values
            self.arrayRef = array.array('f',[])
        
            self.arrayTime = array.array('q',[])
            
            self.MD.disable()
            self.myuart.write('Lab 6 UI Running.\r\n')


        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0 and self.state != self.S0_INIT):

            if (self.state == self.S1_WAIT):  
                
                if self.myuart.any() != 0:
                    
                    self.val = self.myuart.readline().decode('ascii')
                    #print('Putty, msg recieved: {:}'.format(self.val))
                    self.fullval = self.fullval + self.val
                    
                    if 'x' in self.fullval:
                        
                        try :  
                            self.fullval = (self.fullval.replace('x',''))
                            #print('Putty, msg recieved: {:}'.format(self.fullval))                    
                            self.fullval = float(self.fullval)
                            self.myuart.write('\tKp writen as {:}.\r\n'.format(self.fullval))
                            self.CL.setKp(self.fullval)
                            if self.CL.getKp() == self.fullval:
                                self.myuart.write('\tKp sent to closed loop controller.\r\n')
                                self.transitionTo(self.S2_COLLECT)
                                self.begin_time = utime.ticks_us()
                                self.MD.enable()
                                self.next_timeSample = utime.ticks_add(self.begin_time, self.sampleRate)
                            else:
                                self.muuart.write('Error(?), Kp failed to assign itself to closed loop controller.\r\n')
                            self.val = ''
                            self.fullval = ''


                        except : 
                            self.myuart.write('Oi, numbers only, dummy.')
                            self.val = ''
                            self.fullval = ''
                
            if (self.state == self.S2_COLLECT):
                
                
                
                if (utime.ticks_diff(self.curr_time, self.begin_time)) >= 1e6:
                    self.omref = 1200
                else:
                    self.omref = 0
                    
                    
                self.CL.setOmega(self.ENC.get_speed())
                self.CL.setOmegaRef(self.omref)
                
                self.level = self.CL.getLevel()
                self.MD.setDuty(1,self.level)
                
                
                
                ## Updates time for data collection rate        
                self.curr_timeSamples = utime.ticks_us()
    
        
                if(utime.ticks_diff(self.curr_timeSamples, self.next_timeSample) >= 0):
        
                    if self.arrayLengthCounter == 0:
                        self.myuart.write('Sampling in progress - please wait.\r\n')

        
                    self.bufferVal  = self.ENC.get_speed()
                    self.bufferValref = self.CL.getOmegaRef()
                    
                    self.arrayData.append(self.bufferVal)
                    self.arrayRef.append(self.bufferValref)
                    
                    
                    self.bufferTime = (utime.ticks_diff( utime.ticks_us(), self.begin_time ))
                    self.arrayTime.append(self.bufferTime)
                    
                    self.arrayLengthCounter += 1
                    if self.arrayLengthCounter >= self.datapoints:
                        self.transitionTo(self.S3_STOP)
                        self.MD.disable()
                        self.omref = 0
                        self.level = 0
                    self.next_timeSample = utime.ticks_add(self.next_timeSample, self.sampleRate)
        
            if(self.state == self.S3_STOP):
                self.transitionTo(self.S1_WAIT)
                self.terminationStatement = 'Data Collection Completed in {:0.6f} s. \r\n'.format( (utime.ticks_diff(self.curr_time, self.begin_time)) * 1e-6 ) 
                
                for n in range(0,self.arrayLengthCounter): 
                    self.myuart.write( '{:},{:},{:}\r\n'.format( (self.arrayData[n]),(self.arrayRef[n]),(self.arrayTime[n])) )
                
                self.myuart.write(self.terminationStatement)
                                    
                self.arrayData = array.array('f',[])
                self.arrayRef  = array.array('f',[])
                self.arrayTime = array.array('q',[])
                self.arrayLengthCounter = 0
                self.CL.disable()


                        
                

        self.runs += 1
        self.samples += 1
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        # updating the "Scheduled" timestamp
                        
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState   
        
        