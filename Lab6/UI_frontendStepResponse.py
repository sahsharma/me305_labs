## @file UI_frontendStepResponse.py
#  This script is a module that contains UI_frontend classes and methods specifc to Lab 6 to be used in other scripts.
#
#  @package UI_frontendStepResponse
#  This module contains the User interface front end related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 23, 2020

import serial
import numpy as np
import time
#import keyboard
import matplotlib.pyplot as plt



## @brief   The UI Frontend class for lab 6
class UI_FrontLab6:
    
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0
    
    ## Constant defining State 1
    S1_WAIT     = 1
    
    ## Constant defining State 2
    S2_RUN      = 2
    

    ## @brief   Creates the UI_Front object
    def __init__(self, interval):

        ## @brief   Sets up serial communication to COM3
        self.ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)
        
        ## @brief   The array that will hold position data
        self.velArray = 0
        
        ## @brief   The array that will hold reference step resposne
        self.refArray = 0
        
        ## @brief   The array that will hold the time data
        self.timeArray = 0
        
        ## @brief   The line read from the board
        self.lineFromBoard = 0
        
        ## @brief User input object   
        #self.userInput = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT 
        
        ## @brief   The current time that will be updated as the code runs
        self.curr_time = 0
        
        ## read line form the board
        self.lineFromBoard = None
        
        ## Kp value written to the closed loop
        self.Kp = 0
        
        ## Intermediate Kp string to encode before sending over serial
        self.Kpstring = 0
        
    ## @brief runs one iteration fo the task
    def run(self):
    
        ## The current time updated when the code is run
        self.curr_time = time.time()    #updating the current timestamp  

        # State 0 Init
        if (self.state == self.S0_INIT):               
        
            self.transitionTo(self.S1_WAIT)
            self.velArray = np.array([])
            self.refArray = np.array([])
            self.timeArray = np.array([])

        if (self.curr_time >= self.next_time):
        
            if (self.state == self.S1_WAIT):
                
                while self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    print(str(self.lineFromBoard))
                
                
                self.Kp = input('Enter Kp value to test: ')
                # send Kp to the board
                self.Kpstring = '{:}'.format(self.Kp)
                self.ser.write((self.Kpstring+'x').encode('ascii'))
            
                self.Kp = 0
                time.sleep(0.5)
                if self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    print(str(self.lineFromBoard))
                self.transitionTo(self.S2_RUN)
                
            
                
            if (self.state == self.S2_RUN):
                
                #print('waiting')
                if self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    if (',' in str(self.lineFromBoard)) is False:    
                        print(str(self.lineFromBoard))
                elif self.ser.in_waiting == 0:
                    self.lineFromBoard = ''
                if self.ser.out_waiting > 0:
                    self.ser.write()
                    
                if ',' in str(self.lineFromBoard):
                    ## Intermediate variable to store readline split by commas
                    self.splitLine = str(self.lineFromBoard).split(',')
                    #print(self.splitLine)
                    self.velArray = np.append(self.velArray,(float(self.splitLine[0])))
                    self.refArray = np.append(self.refArray,(float(self.splitLine[1])))
                    self.timeArray = np.append(self.timeArray, ((float(self.splitLine[2])*1e-6)))

                if 'Complete' in str(self.lineFromBoard) or 'Stopped' in str(self.lineFromBoard):
                    self.timeArray = self.timeArray - self.timeArray[0]
                    ## Intermediate vairable to stack both time and data arrays together
                    self.arrayForCSV = np.stack((self.velArray, self.refArray ,self.timeArray),axis =-1)
                    
                    #self.plotStuff()
                    plt.plot(self.timeArray,self.velArray, label = "Measured Vel")
                    plt.plot(self.timeArray,self.refArray, label = "Reference Vel")
                    plt.xlabel('Time (s)')
                    plt.ylabel('Rotational Velocity (RPM)')
                    plt.title('Encoder Step Response, Kp = ' + self.Kpstring)
                    plt.legend()
                    plt.grid(True)
                    plt.show()
                    #plt.draw()
                    
                    time.sleep(1)
                    self.velArray = np.array([])
                    self.refArray = np.array([])
                    self.timeArray = np.array([])
                    #self.lineFromBoard = ''

                    
                    plt.savefig('Lab6_StepResponse.png')
                    
                    time.sleep(1)

                    np.savetxt('Lab6_StepResponse.csv',self.arrayForCSV,delimiter=',')
                    
                    self.transitionTo(self.S1_WAIT)

                    
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
            # updating the "Scheduled" timestamp
                    
 
    
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState   