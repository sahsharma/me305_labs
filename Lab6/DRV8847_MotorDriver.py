## @file DRV8847_MotorDriver.py
#  This script is a module that contains the motor driver for the DRV8847 dual H-bridge. The methods here can be used in other scripts to control the bridge.
#
#  @package DRV8847_MotorDriver
#  This module contains the motor driver for the DRV8847 bridge and related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 15, 2020



import pyb
    
    
class Driver:
    ''' This class implements a motor driver for the ME405 board. It is hardcoded to use CPU pins A15, B0, B1, B4, and B5, and uses CPU Timer 3'''
    
    
    def __init__(self,printout=1):
        ''' Creates a motor driver by initializing GPIO pins and turning the motor off for safety.'''
        
        
        ## Enable or disable print outs (printout = 1 to enable, printout = 0 to disable. Enabled by default)
        self.printout = printout
        
        if self.printout == 1:
                print('\tCreating the motor driver.')

        
        ## pin object that drives the nSLEEP pin on the DRV8847
        self.nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP);
        self.nSLEEP_pin.low()
        
        ## pin object that drives the IN1 pin on the DRV8847 (uses CPU pin B4)
        self.IN1_pin = pyb.Pin(pyb.Pin.cpu.B4, mode=pyb.Pin.OUT_PP)
        
        ## pin object that drives the IN2 pin on the DRV8847 (uses CPU pin B5)
        self.IN2_pin = pyb.Pin(pyb.Pin.cpu.B5, mode=pyb.Pin.OUT_PP);
        
        ## pin object that drives the IN3 pin on the DRV8847 (uses CPU pin B0)
        self.IN3_pin = pyb.Pin(pyb.Pin.cpu.B0, mode=pyb.Pin.OUT_PP);
        
        ## pin object that drives the IN4 pin on the DRV8847 (uses CPU pin B1)
        self.IN4_pin = pyb.Pin(pyb.Pin.cpu.B1, mode=pyb.Pin.OUT_PP);
        
        ## timer object for PWM generation (uses CPU Timer 3, frequency of 20kHz)
        self.timer = pyb.Timer(3,freq=20e3);
        
        ## timer 3 channel 1 drives motor 1's negative gate
        self.M1Neg = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin, pulse_width_percent=0)
        
        ## timer 3 channel 2 drives motor 1's positive gate
        self.M1Pos = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin, pulse_width_percent=0)
        
        ## timer 3 channel 3 drives motor 2's negative gate
        self.M2Neg = self.timer.channel(3, pyb.Timer.PWM, pin=self.IN3_pin, pulse_width_percent=0)
        
        ## timer 3 channel 4 drives motor 2's positive gate
        self.M2Pos = self.timer.channel(4, pyb.Timer.PWM, pin=self.IN4_pin, pulse_width_percent=0)

        if self.printout == 1:
                print('\tMotor driver created.')        

    ## @brief Enables the DRV8847 by driving the nSLEEP pin high
    def enable(self):
       
        if self.printout == 1:
            print('\tEnabling Motors.')
        
        self.nSLEEP_pin.high()
        
        if self.nSLEEP_pin.value() == 1:
            if self.printout == 1:
                print('\tMotors Enabled.')
        
    ## @brief Disables the DRV8847 by driving the nSLEEP pin low. Also sets all PWM outputs to 0 duty cycle.
    def disable(self):
        if self.printout == 1:

            print('\tDisabling Motors.')
        
        self.nSLEEP_pin.low()
        self.M1Neg.pulse_width_percent(0)
        self.M1Pos.pulse_width_percent(0)
        self.M2Neg.pulse_width_percent(0)
        self.M2Pos.pulse_width_percent(0)
        
        if self.nSLEEP_pin.value() == 0:
            if self.printout == 1:
                print('\tMotors Disabled.')

        
    def setDuty (self, motor, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param motor integer number targeting motor (e.g. 1 for motor 1)
        @param duty A signed integer holding the duty (-100 to 100)
        cycle of the PWM signal sent to the motor '''
         
        ## Motor number (1 or 2) to apply duty cycle to
        self.motor = motor
        
        ## Duty cycle (-100 to 100)
        self.duty = duty
        if self.printout == 1:
            print('\tSetting motor {:} to {:} duty cycle.'.format(int(self.motor) ,int(self.duty) ))
        
        if self.motor == 1:
            
            if self.duty < 0:
                
                self.M1Pos.pulse_width_percent(0)
                self.M1Neg.pulse_width_percent(self.duty*-1)
                
                if (self.M1Pos.pulse_width_percent() == 0) and (self.M1Neg.pulse_width_percent() == self.duty*-1):
                    if self.printout == 1:
                        print('\tMotor set.')
                 
            elif duty >= 0:
         
                self.M1Pos.pulse_width_percent(self.duty)
                self.M1Neg.pulse_width_percent(0)
                
                if (self.M1Pos.pulse_width_percent() == self.duty) and (self.M1Neg.pulse_width_percent() == 0):
                    if self.printout == 1:
                        print('\tMotor set.')                
                
        if self.motor == 2:
            
            if self.duty < 0:
                
                self.M2Pos.pulse_width_percent(0)
                self.M2Neg.pulse_width_percent(self.duty*-1)
                
                if (self.M2Pos.pulse_width_percent() == 0) and (self.M2Neg.pulse_width_percent() == self.duty*-1):
                    if self.printout == 1:
                        print('\tMotor set.')       
                    
            elif duty >= 0:
         
                self.M2Pos.pulse_width_percent(self.duty)
                self.M2Neg.pulse_width_percent(0)
                        
                if (self.M2Pos.pulse_width_percent() == self.duty) and (self.M2Neg.pulse_width_percent() == 0):
                    if self.printout == 1:
                        print('\tMotor set.')             
        
        if __name__ == '__main__':
            # Adjust the following code to write a test program for your motor class. Any
            # code within the if __name__ == '__main__' block will only run when the
            # script is executed as a standalone program. If the script is imported as
            # a module the code block will not run.
           
           
            # Create a motor object passing in the pins and timer
            moe = Driver()
           
            # Enable the motor driver
            moe.enable()
           
            # Set the duty cycle to 40 percent
            moe.set_duty(1,40)