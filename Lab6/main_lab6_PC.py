## @file main_lab6_PC.py
#  This script runs the frontend for Lab 6
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 23, 2020



from UI_frontendStepResponse import UI_FrontLab6


## Set interval to run frontend at in seconds
Interval = 1e-4

## Build Lab6 frontend
task1 = UI_FrontLab6(Interval)
    

## Run forever
while True:
    task1.run()
