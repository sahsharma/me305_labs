## @file main.py
# This is the main file that calls the LED blinking state machine
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 13, 2020

from ME305_Lab2 import VirtBlink, VirtLED, LD2_LED
#from ME305_Lab2_PWM import LD2_LED

## VirtLED Object
LED1 = VirtLED('PB5')

## Interval object virtual led
Interval1 = 1

## Interval object for PWM generator
Interval2 = (10e-6)

## Frequency Object for the PWM generator
Frequency = 20e3

## Period Object for the target waveform
Period = 10

## Transition object to move between sinusoid and sawtooth
Transition = 30

## Task1 Object that holds the Virtual LED Blinker 
task1 = VirtBlink(LED1, Interval1)
## Task2 Object that holds the PWM Generator (Hardcoded to PinA5 and Timer2)
task2 = LD2_LED(Frequency,Period,Transition,Interval2)

while True:
    task1.run()
    task2.run()