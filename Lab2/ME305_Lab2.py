## @file ME305_Lab2.py
#  This script simulates control of a virtual LED and controls the LED on the Nucleo using a finite state machine 
#
#  @package ME305_Lab2
#  This module contains the finite-state machines for a virtual LED and the physcial LED on the Nucleo 
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 13, 2020

import utime
import pyb
import math

#============================================================================#
#============================================================================#

## @brief   A finite state machine to blink a virtual LED in the REPL
class VirtBlink:
    ## @details     This FSM toggles between an ON and OFF state
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0
    
    ## Constant defining State 1
    S1_LED_ON             = 1
     
    ## Constant defining State 2
    S2_LED_OFF            = 2
    
    ## @brief   Creates a virtual LED object
    def __init__(self, LED, interval):
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A virtual LED object
        self.LED = LED
                
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        
    ## @brief   runs one iteration of the task
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp
        
                    
        # State 0 Init
        if(self.state == self.S0_INIT):
            print('VirtLED: Interval set for ' + str((self.interval*1e-6)) + ' s')
            print('VirtLED: ' + str(self.runs) + ' | {:0.6f} | State 0 Init'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
            # Run State 0 Code

            self.transitionTo(self.S1_LED_ON)
            self.LED.setLED(0)
                    
            
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0 and self.state != self.S0_INIT):

            # State 1 LED ON
            if(self.state == self.S1_LED_ON):
                print('VirtLED: ' + str(self.runs) + ' | {:0.6f} | State 1'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
                # Run State 1 Code

                self.transitionTo(self.S2_LED_OFF)            
                self.LED.setLED(1)


            # State 2 LED OFF
            elif(self.state == self.S2_LED_OFF):
                print('VirtLED: ' + str(self.runs) + ' | {:0.6f} | State 2'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
                # Run State 2 Code

                self.transitionTo(self.S1_LED_ON)            
                self.LED.setLED(0)

        
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp

        
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState        
        
#============================================================================#
#============================================================================#
        
## @brief   A finite state machine to control the LED on the Nucleo with PWM
class LD2_LED:
    ## @details     The FSM uses PWM to generate sinusoidal and sawtooth approximations
    #               to change the power delivered to the LD2 LED on the Nucleo.
    #               The FSM is hardcoded to the PA5 pin and timer 2.
    
    ## Constant defining State 0 - Initialization
    S0_INIT               = 0
    
    ## Constant defining State 1
    S1_SINE               = 1
     
    ## Constant defining State 2
    S2_SAW                = 2
    
    ## @brief   Creates a LD2 Driver object
    def __init__(self, frequency, period, transition, interval):
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## PWM Frequency object
        self.frequency = frequency
        
        ## Period of waveform to replicate object
        self.period = int(period*1e6)
        
        ## Transition between states
        self.transition = int(transition*1e6)
                
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

                
    
    ## @brief   runs one iteration of the task
    def run(self):
        
        #global duty
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        #print('current time: ' + str(self.curr_time))
        # updating the current timestamp
        global t2ch1
                    
        # State 0 Init
        if(self.state == self.S0_INIT):
            print('PWM: Interval set for ' + str(self.interval) + ' us')
            print('PWM: Freq. set for ' + str(self.frequency) + ' Hz')
            print('PWM: Period to replicate set for ' + str((self.period*1e-6)) + ' s')
            print('PWM : ' + str(self.runs) + ' | {:0.6f} | State 0 Init'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
            # Run State 0 Code

            self.transitionTo(self.S1_SINE)
            ## time since new state transition
            self.newstate_time = utime.ticks_diff(self.curr_time,self.start_time)
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = self.frequency)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            t2ch1.pulse_width_percent(0)
                    
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0 and self.state != self.S0_INIT):

            # State 1 Sinusoid
            if(self.state == self.S1_SINE):
                #print('PWM: ' + str(self.runs) + ' | {:0.4f} | State 1 Sinusoid'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
                # Run State 1 Code
                                
                if utime.ticks_diff(self.transition,( utime.ticks_diff((utime.ticks_diff(self.curr_time,self.start_time)),self.newstate_time) )) <= 0:
                    self.transitionTo(self.S2_SAW)
                    self.newstate_time = utime.ticks_diff(self.curr_time,self.start_time)
                
                happy = Duty().Sine( period=(self.period*1e-6), time=(utime.ticks_diff(self.curr_time,self.start_time))*1e-6 , amp=0.5, offset=0.5 ) 
                t2ch1.pulse_width_percent(100*happy)

                
            
            # State 2 Sawtooth
            elif(self.state == self.S2_SAW):
                #print('PWM: ' + str(self.runs) + ' | {:0.4f} | State 2 Sawtooth'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
                # Run State 2 Code

                
                if (utime.ticks_diff(self.transition,( utime.ticks_diff((utime.ticks_diff(self.curr_time,self.start_time)),self.newstate_time) ))) <= 0:
                    self.transitionTo(self.S1_SINE)
                    self.newstate_time = utime.ticks_diff(self.curr_time,self.start_time)

                dopey = Duty().Saw( period=(self.period*1e-6), time=(utime.ticks_diff(self.curr_time,self.start_time))*1e-6, amp=0.5, offset=0.5 )
                t2ch1.pulse_width_percent(100*dopey)

            
        
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp

        
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState    
    
    
#============================================================================#
#============================================================================#

## @brief       A virtual LED class to drive a virtual LED
## @details     This class is connected to psuedo-hardware and will only output
#               strings of "LED ON" and "LED OFF" to the console or REPL.
class VirtLED:
    
    ##  @brief              Creates an LED object
    #   @param pin          A virtual pin object that the button is connected to
    def __init__(self, pin):
        
        ## pin object
        self.pin = pin
        print('Virtual LED object created attached to "pin '+ str(self.pin) + '"')
    
    ## @brief       Creates a set LED method
    #  @param hilo  A hilo parameter that asserts a high or low state to the virtual LED    
    def setLED(self, hilo):
        
        ## hilo object
        self.hilo = hilo
        if self.hilo == 0:
            print('~LED OFF')
        if self.hilo == 1:
            print('~LED ON')
            
            
#============================================================================#
#============================================================================#

## @brief       Duty cycle object
class Duty:
    
    ##  @brief              Creates a Duty Cycle object
    def __init__(self):
        
        pass
    
    ## @brief            Creates a sinusoid duty cycle mthod
    #  @param period     A period parameter in seconds
    #  @param time       A time parameter
    #  @param amp        An amplitude parameter   
    #  @param phase      A phase parameter in radians
    #  @param offset     A vertical offset parameter
    def Sine(self, period, time, amp, phase = 0, offset = 0):
        
        ## amp object        
        self.amp = amp
        
        ## period object
        self.period = period
        
        ## time object
        self.time = time
        
        ## phase object        
        self.phase = phase
        
        ## vertical offset object
        self.offset = offset
        
        
        ## Magnitude of the sinusoid plot
        sinMag = ((self.amp)*math.sin( (2*math.pi*(1/self.period)*self.time) + phase )) + offset
        return sinMag
    
    ## @brief            Creates a sinusoid duty cycle mthod
    #  @param period     A period parameter in seconds
    #  @param time       A time parameter
    #  @param amp        An amplitude parameter   
    #  @param offset     A vertical offset parameter
    def Saw(self, period, amp, time, offset = 0):
        
        
        ## amp object
        self.amp = amp
        
        ## period object
        self.period = period
        
        ## time object
        self.time = time
        
        ## vertical offset object
        self.offset = offset
            
        
        ## Magnitude of the sawtooth wave
        sawMag = self.amp*(( (((self.time) % (self.period)) - (self.period/2)) / (self.period/2) )) + (self.offset)
        return sawMag