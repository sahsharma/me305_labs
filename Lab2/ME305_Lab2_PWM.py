   ## @file ME305_Lab2_PWM.py
#  This script hold the FSM for controlling the LED on the Nucleo
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 13, 2020

import utime
import pyb
import math

     
## @brief   A finite state machine to control the LED on the Nucleo with PWM
class LD2_LED:
    ## @details     The FSM uses PWM to generate sinusoidal and sawtooth approximations
    #               to change the power delivered to the LD2 LED on the Nucleo.
    #               The FSM is hardcoded to the PA5 pin and timer 2.
    
    ## Constant defining State 0 - Initialization
    S0_INITI               = 0
    
    ## Constant defining State 1
    S1_SINE               = 1
     
    ## Constant defining State 2
    S2_SAW                = 2
    
    ## @brief   Creates a LD2 Driver object
    def __init__(self, frequency, period, transition, interval):
        
        ## The state to run on the next iteration of the task.
        self.states = self.S0_INITI
        
        ## PWM Frequency object
        self.frequency = frequency
        
        ## Period of waveform to replicate object
        self.period = int(period*1e6)
        
        ## Transition between states
        self.transition = int(transition*1e6)
                
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

                
    
    ## @brief   runs one iteration of the task
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp
        
                    
        # State 0 Init
        if(self.states == self.S0_INITI):
            print('PWM: Interval set for ' + str(self.interval) + ' us')
            print('PWM: Freq. set for ' + str(self.frequency) + ' Hz')
            print('PWM: Period to replicate set for ' + str(self.period) + ' s')
            print(str(self.runs) + ' | {:0.4f} | State 0 Init'.format(utime.ticks_diff(self.curr_time, self.start_time)))
            # Run State 0 Code

            self.transitionTo(self.S1_SINE)
            # time since new state transition
            self.newstate_time = utime.ticks_diff(self.curr_time,self.start_time)
            pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
            tim2 = pyb.Timer(2, freq = self.frequency)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                    
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0 and self.state != self.S0_INITI):

            # State 1 Sinusoid
            if(self.states == self.S1_SINE):
                print('PWM: ' + str(self.runs) + ' | {:0.4f} | State 1 Sinusoid'.format(utime.ticks_diff(self.curr_time, self.start_time)))
                # Run State 1 Code
                
                if utime.ticks_diff(self.transition,( utime.ticks_diff((utime.ticks_diff(self.curr_time,self.start_time)),self.newstate_time) )) <= 0:
                    self.transitionTo(self.S2_SAW)
                    self.newstate_time = utime.ticks_diff(self.curr_time,self.start_time)
                
                duty = ( (50*math.sin(2*math.pi*(1/self.period)*utime.ticks_diff(self.curr_time,self.start_time)))+50 )
                t2ch1.pulse_width_percent(duty)
                
            
            # State 2 Sawtooth
            elif(self.states == self.S2_SAW):
                print('PWM: ' + str(self.runs) + ' | {:0.4f} | State 2 Sawtooth'.format(utime.ticks_diff(self.curr_time, self.start_time)))
                # Run State 2 Code
                
                if utime.ticks_diff(self.transition,( utime.ticks_diff((utime.ticks_diff(self.curr_time, self.start_time)),self.newstate_time) )) <= 0:
                    self.transitionTo(self.S1_SINE)
                    self.newstate_time = utime.ticks_diff(self.curr_time,self.start_time)
                duty = ( 100 * ((( utime.ticks_diff( utime.ticks_diff(self.curr_time,self.start_time) , self.newstate_time ) % (self.period)))/self.period) )
                t2ch1.pulse_width_percent(duty)

        
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp

        
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState    
    
    