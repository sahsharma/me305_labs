## @file Encoder.py
#  This script is a module that contains Encoder classes and methods to be used in other scripts.
#
#  @package Encoder
#  This module contains the Encoder related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 20, 2020


import pyb
import utime

#============================================================================#
#============================================================================#

## @brief   An EncoderAB class
class EncoderAB:
    ## @details      to configure a timer as an encoder counter for quadrature encoders
    
    ## @brief    Creates the EncoderAB object
    def __init__(self, timer, prescaler, period, channelA, pinA, channelB, pinB, CPR, reduction, interval):
        
        ## @brief   timer object to represent which Timer on the uC to use
        self.timer = int(timer)
        
        ## @brief   prescaler object
        self.prescaler = prescaler
        
        ## @brief   periof object
        self.period = period
        
        ## @brief   channelA object
        self.channelA = int(channelA)
        
        ## @brief   pinA object
        self.pinA = pinA
        
        ## @brief   channelB object
        self.channelB = int(channelB)
        
        ## @brief   pinB object
        self.pinB = pinB
        
        ## @brief   CPR object.
        self.CPR = CPR
            ## @details     Refers to the counts per revolution specific to the encoder
        
        ## @brief   reduction object.
        self.reduction = reduction
            ## @details     Refers to gear reductions between the encoder and the output shaft of the motor
        
        ## @brief   counter2 object
        self.counter2 = 0
        
        ## @brief   counter1 object
        self.counter1 = 0
        
        ## @brief   current_position object
        self.current_position = 0
        
        ## @brief   delta1 object
        self.delta1 = 0
        
        ## @brief   delta2 object
        self.delta2 = 0
        
        ## @brief   speed object
        self.speed = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## @brief   current time
        self.curr_time = 0
        
        ## @brief   previous timestamp
        self.last_time1 = 0
        
        ## @brief   previous previous timestamp
        self.last_time2 = 0

        ## @brief   timer object
        self.tim = pyb.Timer(self.timer)
        self.tim.init(prescaler=self.prescaler, period=self.period)
        self.tim.channel(self.channelA, pin=self.pinA, mode=pyb.Timer.ENC_AB)
        self.tim.channel(self.channelB, pin=self.pinB, mode=pyb.Timer.ENC_AB)
        
    ## @brief   runs one iteration of the task
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp
            
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            self.last_time2 = self.last_time1
            self.last_time1 = self.curr_time
            self.update()
            #t = self.update()
            #print('Encoder | ' + str(self.runs) + ' | {:0.4f} \n   '.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ) + str(t))
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp


    ## @brief   update method
    def update(self):
        ## @details     updates the value of the current_postion object by adding filtered values of delta
        
        self.counter2 = self.counter1
        self.counter1 = self.tim.counter()
        
                    
        self.delta1 = self.counter1 - self.counter2 
        
        #overflow forwards
        if self.delta1 < (-0.5*self.period):
            self.delta2 = self.delta1 + (self.period + 1)
        
        #underflow backwards
        elif self.delta1 > (0.5*self.period):
            self.delta2 = self.delta1 - (self.period + 1)
        
        #no change, already good delta
        else:
            self.delta2 = self.delta1
        
        #add good deltas
        self.current_position = self.current_position + self.delta2
        return self.current_position
        
    ## @brief   get_position method
    def get_position(self):
        ## @details     returns the value of the current_position object
        
        return self.current_position
    
    ## @brief   set_postion method
    def set_position(self, pos):
        ## @details     passes the argument to current_postion object and overwrites it
        
        self.current_position = pos
        #return self.current_position
        #self.counter1 = 0
        #self.counter2 = 0
        #self.delta = 0
        
    ## @brief   get_delta object
    def get_delta(self):
        ## @details     returns the value of the filtered delta2 object
        
        return self.delta2
    
    ## @brief   get_speed object
    def get_speed(self):
        ## @details     uses the filtered delta2, CPR, and reduction objects to calculate the RPM of the motor's output shaft and returns the value
        
        # in rpm
        self.speed = ((((self.delta2 / ((utime.ticks_diff(self.last_time1, self.last_time2))*1e-6)) / (self.CPR)) * 60) / (self.reduction))
        return self.speed
        #return  ((utime.ticks_diff(self.last_time1, self.last_time2)))
        
        
#============================================================================#
#============================================================================#        