## @file UI.py
#  This script is a module that contains User Interface (UI) classes

#  @package UI
#  This module contains the user interface classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 20, 2020


from pyb import UART
import utime


#============================================================================#
#============================================================================#

## @brief   UI_Encoder class
class UI_Encoder:
    ## @details Sets up the user interface to communicate with an EncoderAB object

    ## Constant defining State 0 - Initialization
    S0_INIT               = 0
    
    ## Constant defining State 1
    S1_RUN             = 1
    
    ## @brief   Creates a UI_Encoder object
    def __init__(self, bus, Enc, interval):

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT        

        ## bus object to specify which UART bus to use
        self.bus = bus
        
        ## Enc object specifies which EncoderAB object to link to
        self.Enc = Enc
        
        ## interval object to specify rate to run at
        self.interval = interval
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## UART object
        self.uart = UART(2)        
        
    ## @brief   runs one iteration of the task
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp
         
        if(self.state == self.S0_INIT):
            self.transitionTo(self.S1_RUN)
            self.uart.write('Encoder UI Running | {:0.6f} | Type the following lowercase characters:\n\r\td - Print out the encoder delta\n\r\tp - Print out encoder position\n\r\tz - Zero the encoder position\n\r\ts - Print output shaft speed\n\r'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0) & (self.state != self.S0_INIT):
            # runtime code            
            
            if self.uart.any() > 0 :
                
                ## @brief user inputed character
                userinput  = self.uart.readchar()
                
                if userinput == 0x7A: # z
                    # Zero the encoder position
                    self.Enc.set_position(pos = 0)
                    self.uart.write('UI: Encoder zeroed. | {:0.6f} \n\r   '.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ) + '\n\r')
                
                elif userinput == 0x70: # p
                    # Print out the encoder position
                    self.uart.write('UI: Encoder position | {:0.6f} | : \n\r   '.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ) + str(self.Enc.get_position()) + '\n\r')
                    
                    
                elif userinput == 0x64: # d
                    # Print out the encoder delta
                    self.uart.write('UI: Encoder delta | {:0.6f} | : \n\r   '.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ) + str(self.Enc.get_delta()) + '\n\r')


                elif userinput == 0x73: # s
                    # Print out the output shaft speed in +/-RPM
                    self.uart.write('UI: Motor Speed | {:0.6f} | : \n\r   {:0.2f}'.format( ((utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ),(self.Enc.get_speed())) + ' RPM\n\r')
                    #self.uart.write('UI: Speed | {:0.4f} | : \n\r   {}'.format( ((utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ),(self.Enc.get_speed())) + '\n\r')
                    
                else:
                    self.uart.write('UI: Invalid character; Use the characters listed above. | {:0.4f} \n\r'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
                    
                    

            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp        

    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState      
        
#============================================================================#
#============================================================================#