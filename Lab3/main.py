## @file main.py
# This is the main file that calls EncoderAB class and UI_Encoder class and runs both tasks.
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 18, 2020

import pyb
from Encoder import EncoderAB
from UI import UI_Encoder

## Set Timer object
Timer = 3

## Set Prescaler object
Prescaler = 0

## Set Period object
Period = 0xFFFF

## Set ChannelA object
ChannelA = 1

## Set ChannelB object
ChannelB = 2

## Set PinA object
PinA = pyb.Pin.cpu.A6

## Set PinB object
PinB = pyb.Pin.cpu.A7

## Set CPR object
CPR = 7*4

## Set Reduction object
Reduction = 50

## Set Interval1 Object
Interval1 = (1000)*1e-6

## Build task1 object using EncoderAB class
task1 = EncoderAB(Timer, Prescaler, Period, ChannelA, PinA, ChannelB, PinB, CPR, Reduction, Interval1)

#============================================================================#
#============================================================================#

## Set Bus object
Bus = 2

## Set Interval2 object
Interval2 = 40*1e-6

## Build task2 object using UI_Encoder class
task2 = UI_Encoder(Bus, task1, Interval2)

## Run indefinitly
while True:
    task1.run()
    task2.run()
