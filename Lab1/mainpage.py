## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is Lab 0x01 of ME 305. In this lab, we created a python program that calculates Fibonacci number for a given index.
#  It is intended to be imported as a module and executed using the fib method.
#  The code filters out nonintegers and any nonpostive integer entries.
#  This is the second coded version (hence the V2 suffix). This version uses memoization rather than filling out arrays.
#
#  @section sec_fib fib Method
#  The fib method is takes an integer argument and calcualtes its Fibonacci number. Please see ME305_lab1_V2.fib which is part of the \ref ME305_lab1_V2 package.
#
#  @section page_fib_src Source Code Access
#  You can find the source code for the main python file here:
#  https://bitbucket.org/sahsharma/me305_labs/src/master/Lab1/ME305_lab1_V2.py
#  
#  HTML files are found here:
#  https://bitbucket.org/sahsharma/sahsharma.bitbucket.io/src/master/  
#  
#    
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date September 28, 2020
