## @file ME305_lab1_V2.py
#  This script controls a virtual LED
#
#  This is to be run on a computer and demonstrates a 
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 13, 2020
#
#  @package ME305_lab1_V2
#  Brief doc for the ME305_lab1_V2 module second
#
#  Detailed doc for the ME305_lab1_V2 module second
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date September 28, 2020

import numpy as np


## The fib method takes an argument (idx) and calculates its Fibonacci number. If anything other than a nonnegative integer (like a string, a demical number, or a negative value) are entered, it returns an error statement.
#  @param idx An integer specifying the index of the desired Fibonacci number.
def fib(idx):
    
    fail = 'Please enter a nonnegative integer.';    # fail statement
    
    if (type(idx) == int):          # test if input is an integer
        if (idx >= 0):              # also test if the integer is nonnegative
            print ('   Calculating Fibonacci number at '
                   'index n = {:}.'.format(idx))
            
            # initialize sequence
            if idx == 0:
                output = 0
            elif idx == 1:
                output = 1
            else:                   # set up an array to map out the sequence
                indicies = np.linspace(0,idx,idx+1, dtype = np.int64)
                mem1 = 1
                mem2 = 0
                for ind in indicies[2::]:        # loop through sequence
                    mem = mem1 + mem2
                    mem2 = mem1
                    mem1 = mem
                output = mem
            
            print ('   Output is {}.'.format(output))
            
        else:                       # input is an integer, but is negative
            print(fail)
            del idx           
    else:                           # input is not an integer
        print(fail)
        del idx


## Code used for running & testing the script as a standalone program
if __name__ == '__main__':
    while True:    
## Variable representing the user inputed data after being prompted. The variable's data type is evaluated to check if it's a nonnegative integer.
        inde = input('Enter a nonnegative integer to calculate its Fibonacci number: ')
        if (inde.isnumeric() == True):
            if (int(inde) != 0):
                inde = int(inde)
                fib(inde)
            else:
                print('Error - only enter nonnegative integers.')
                print('Restarting program.')
                del inde
        else:
            print('Error - only enter nonnegative integers.')
            print('Restarting program.')