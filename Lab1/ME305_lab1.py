# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 11:44:48 2020

@author: sahil
@file ME305_lab1.py

"""
import numpy as np


def fib(idx):
    ''' this method calcuates a Fibonacci number corresponing
    to a specified index.
    @param idx An integer specifying the index of the desired
        Fibonacci number.'''
    
    
    fail = 'Please enter a nonnegative integer.';    # fail statement
    
    if (type(idx) == int):          # test if input is an integer
        if (idx >= 0):              # also test if the integer is nonnegative
            print ('   Calculating Fibonacci number at '
                   'index n = {:}.'.format(idx))
            
            # initialize sequence
            if idx == 0:
                output = 0
            elif idx == 1:
                output = 1
            else:                   # set up an array to map out the sequence
                array = np.zeros((idx+1,2), dtype = np.int64)
                indicies = np.linspace(0,idx,idx+1, dtype = np.int64)
                array[:,0] = indicies
                array[1,1] = 1
                for ind in array[2::,0]:        # loop through sequence
                    array[ind,1] = array[ind-1,1] + array[ind-2,1]
                output = array[ind,1]
            
            print ('   Output is {}.'.format(output))
            
        else:                       # input is an integer, but is negative
            print(fail)
            del idx
            
    else:                           # input is not an integer
        print(fail)
        del idx

            
if __name__ == '__main__':
    while True:    
        inde = input('Enter a nonnegative integer to calculate its Fibonacci number: ')
        if (inde.isnumeric() == True):
            if (int(inde) != 0):
                inde = int(inde)
                fib(inde)
            else:
                print('Error - only enter nonnegative integers.')
                print('Restarting program.')
                del inde
        else:
            print('Error - only enter nonnegative integers.')
            print('Restarting program.')
            del inde