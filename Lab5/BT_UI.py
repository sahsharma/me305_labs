## @file BT_UI.py
#  This script is a module that contains BLuetooth User Interface classes and methods to be used in other scripts.
#
#  @package BT_UI
#  This module contains the Bluetooth User Interface related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 10, 2020


import utime


## @brief   The Bluetooth UI class
class BT_UI:
    ## @details   Associates the bluetooth driver with 
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    
    ## Constant defining State 1
    S1_WAIT = 1
    
    ## @brief    Creates the Bluetooth UI object
    def __init__(self, interval, Blue):
    
        ## The Blutooth Driver object to link to
        self.BLE = Blue
        
        ## The value that saves the UART readline result
        self.val = 0
        
        ## Holds the converted to float version of the result
        self.result = 0
        
        ## Concatinated version of the all the UART readlines
        self.fullval = b''

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT

        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

    ## @brief   Runs one iteration of the task
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp
        
                    
        # State 0 Init
        if(self.state == self.S0_INIT):
            
            self.transitionTo(self.S1_WAIT)
            self.BLE.write('BT_UI initialized.')
            
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0 and self.state != self.S0_INIT):
                
            
            if self.BLE.anyBuff() != 0:
                
                self.val = self.BLE.read()
                #print('Putty, msg recieved: {:}'.format(self.val))
                self.fullval += self.val
                
                if 'x' in self.fullval.decode('ascii'):
                    
                    self.fullval = (self.fullval.decode('ascii')).replace('x','')
                    #print('Putty, msg recieved: {:}'.format(self.fullval))
                    
                    try :  
                        self.result = float(self.fullval)
                        self.BLE.setBlink(self.result)
                        self.BLE.write('Blinker set to {:} Hz.'.format(self.result))
                        #print('Putty, blinker set to {:} Hz.'.format(self.result))
                        self.fullval = b''
                    except : 
                        self.BLE.write('Oi, numbers only.')
                        self.fullval = b''

            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # updating the "Scheduled" timestamp
    
        
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState        