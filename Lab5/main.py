## @file main.py
#  This script is runs the bluetooth UI for lab 5
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 10, 2020




import BLE
from BT_UI import BT_UI

## Build a BluetoothDriver object
Blue = BLE.BluetoothDriver()

## Set interval to run at
Interval1 = 4000e-6

## Build user interface connected to bluetooth driver
task1 = BT_UI(Interval1,Blue)

while True:
    task1.run()
