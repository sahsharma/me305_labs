import pyb
from pyb import UART

uart = UART(3,9600)

pinA5 = pyb.Pin(pyb.Pin.cpu.A5)


while True:
    if uart.any() != 0:
        val = int(uart.readline())
        if val == 0:
            print(val, ' turns light OFF')
            pinA5.low()
        elif val == 1:
            print(val, ' turns light ON' )
            pinA5.high()
            


