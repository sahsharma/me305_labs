## @file BLE.py
#  This script is a module that contains BLE Driver classes and methods to be used in other scripts.
#
#  @package BLE
#  This module contains the BLE Driver related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 10, 2020



import pyb
from pyb import UART


## @brief   Bluetooth Driver class
class BluetoothDriver:
    ## @details     Sets up UART channel 3 to connect to the HC-05 Bluetooth module and sets up pinA5 and Timer 2 for PWM-driven blinky stuff

    ## @brief   Builds the bluetooth driver object hardcoded to specific channels and pins due to semi-permanant hardware
    def __init__(self):
        
        ## @brief  The UART object, channel 3 and 9600 baud rate
        self.myuart = UART(3,9600)

        ## The pinA5 set up
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

        ## Frequency to set the blinky LED at, default to 1Hz on bootup for SOL
        self.freq = 1

        ## Timer 2 set up
        self.tim2 = pyb.Timer(2, freq = self.freq)
        
        ## Timer 2, channel 1 PWM set up for pinA5
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
        self.t2ch1.pulse_width_percent(50)

    ## @brief   UART.readline 
    def read(self):
        
        return self.myuart.readline()
        
    ## @brief   UART.write
    def write(self,stuffToWrite):
        
        self.myuart.write(str(stuffToWrite))
    
    ## @brief   UART.any
    def anyBuff(self):
        
        return self.myuart.any()
        
    ## @brief   Reinitializes timer frequency and reasserts pulse width percent to 50
    def setBlink(self,freq):
        
        self.freq = freq
        self.tim2.init(freq=self.freq)
        self.t2ch1.pulse_width_percent(50)

        