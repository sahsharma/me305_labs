## @file main_lab7_PC.py
#  This script runs the frontend for Lab 7
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date December 3, 2020



from UI_frontendRefTracker import UI_FrontLab7


## Set interval to run frontend at in seconds
Interval = 1e-4

## Build Lab6 frontend
task1 = UI_FrontLab7(Interval)
    

## Run forever
while True:
    task1.run()
