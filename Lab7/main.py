## @file main.py
#  This script runs the backend for lab 7
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date December 3, 2020



import pyb
from Encoder import EncoderAB
from UI_detaGenRefTracker import RefTrackLab7
from ClosedLoop import CL
import DRV8847_MotorDriver


## Set Timer object
Timer = 4

## Set Prescaler object
Prescaler = 0

## Set Period object
Period = 0xFFFF

## Set ChannelA object
ChannelA = 1

## Set ChannelB object
ChannelB = 2

## Set PinA object
PinA = pyb.Pin.cpu.B6

## Set PinB object
PinB = pyb.Pin.cpu.B7

## Set CPR object
CPR = 1000*4

## Set Reduction object
Reduction = 1

## Set Interval1 Object
Interval1 = (1000)*1e-6

## Build task1 object using EncoderAB class
task1 = EncoderAB(Timer, Prescaler, Period, ChannelA, PinA, ChannelB, PinB, CPR, Reduction, Interval1)



## Set interval to run closed loop proportional controller at
Interval2 = 40*(1e-6)

## Build the closed loop prop. controller
task2 = CL(Interval2)



## UART object
myuart = 2

## Total time to sample over
sampleTime = 15 # total time to sample

## Sample rate in Hz
sampleRate = 100 # in Hz

## Total number of data points
datapoints = sampleTime*sampleRate

## Build Motor Driver object
MD = DRV8847_MotorDriver.Driver(printout=0)

## Interval to run the interface code (backend)
Interval3 = 40*(1e-6)

## Build the interface (backend)
task3 = RefTrackLab7(myuart,task1,task2,MD,datapoints,sampleRate,Interval3)



while True:
    task1.run()
    task2.run()
    task3.run()
    