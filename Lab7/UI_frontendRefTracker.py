## @file UI_frontendRefTracker.py
#  This script is a module that contains...
#
#  @package UI_frontendRefTracker
#  This module contains...
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date December 3, 2020

import serial
import numpy as np
import time
#import keyboard
import matplotlib.pyplot as plt



## @brief   The UI Frontend class for lab 7
class UI_FrontLab7:
    
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0
    
    ## Constant defining State 1
    S1_WAIT     = 1
    
    ## Constant defining State 2
    S2_RUN      = 2
    

    ## @brief   Creates the UI_Front object
    def __init__(self, interval):

        ## @brief   Sets up serial communication to COM3
        self.ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)
        
        ## @brief   The array that will hold velocity data
        self.velArray = 0
        
        ## @brief   The array that will hold reference velocity
        self.refArray = 0
        
        ## @brief   The array that will hold position data
        self.posArray = 0
        
        ## @brief   The array that will hold the time data
        self.timeArray = 0
        
        ## @brief   The line read from the board
        self.lineFromBoard = 0
        
        ## @brief User input object   
        #self.userInput = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT 
        
        ## @brief   The current time that will be updated as the code runs
        self.curr_time = 0
        
        ## read line form the board
        self.lineFromBoard = None
        
        ## Kp value written to the closed loop
        self.Kp = 0
        
        ## Intermediate Kp string to encode before sending over serial
        self.Kpstring = 0
        
        ## Input from the board stating which time to lookup in the CSV data
        self.timeQuery = ''
        
        ## Index of the value that closest matches the timeQuery
        self.index = 0 

    ## @brief runs one iteration fo the task
    def run(self):
    
        ## The current time updated when the code is run
        self.curr_time = time.time()    #updating the current timestamp  

        # State 0 Init
        if (self.state == self.S0_INIT):               
        
            self.transitionTo(self.S1_WAIT)
            self.velArray = np.array([])
            self.refArray = np.array([])
            self.timeArray = np.array([])
            self.posArray = np.array([])
            
            ## CSV time data saved to array
            self.csvTime = np.array([])
            ## CSV velocity data saved to array
            self.csvVel = np.array([]) 
            ## CSV position data saved to array
            self.csvPos = np.array([])
           
            ## csvFile object that opens the reference CSV doc for reading
            self.csvFile = open('reference.csv')
            while True:
                ## csvLine object that hold the read line data from the file
                self.csvLine = self.csvFile.readline()
                if self.csvLine == '':
                    break
                else:
                    (self.col1,self.col2,self.col3) = self.csvLine.strip().split(',');
                    self.csvTime = np.append(self.csvTime,float(self.col1))
                    self.csvVel = np.append(self.csvVel,float(self.col2))
                    self.csvPos = np.append(self.csvPos,float(self.col3))
            self.csvFile.close()
                
            print(self.csvTime)
                

        if (self.curr_time >= self.next_time):
        
            if (self.state == self.S1_WAIT):
                
                while self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    print(str(self.lineFromBoard))
                
                
                self.Kp = input('Enter Kp value to test: ')
                # send Kp to the board
                self.Kpstring = '{:}'.format(self.Kp)
                self.ser.write((self.Kpstring+'x').encode('ascii'))
            
                self.Kp = 0
                time.sleep(0.5)
                if self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    print(str(self.lineFromBoard))
                self.transitionTo(self.S2_RUN)
                
            
                
            if (self.state == self.S2_RUN):
                
                #print('waiting')
                if self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    if (',' in str(self.lineFromBoard)) is False and ('x' in str(self.lineFromBoard)) is False:    
                        print(str(self.lineFromBoard))
                #elif self.ser.in_waiting == 0:
                #    self.lineFromBoard = ''
                if self.ser.out_waiting > 0:
                    self.ser.write()
                    
                if ',' in str(self.lineFromBoard):
                    ## Intermediate variable to store readline split by commas
                    print(self.lineFromBoard)
                    try:
                            
                        self.splitLine = str(self.lineFromBoard).split(',')
                        self.posArray = np.append(self.posArray, (float(self.splitLine[0])) )
                        self.velArray = np.append(self.velArray,(float(self.splitLine[1])))
                        self.refArray = np.append(self.refArray, (float(self.splitLine[2])) )
                        self.timeArray = np.append(self.timeArray, ((float(self.splitLine[3])*1e-6)))
                    except:
                        self.lineFromBoard = self.ser.readline().decode('ascii')
                        print(self.lineFromBoard)
                        
                    
                if ('x' in str(self.lineFromBoard)):
                    try:
                        self.lineFromBoard = self.lineFromBoard.replace('x','')
                        self.timeQuery = float(self.lineFromBoard)*1e-6
                        time.sleep(0.001)
                        #print(self.timeQuery)
                        self.index =  (np.abs(self.csvTime - self.timeQuery)).argmin()
                        # print(self.index)
                        # print(self.csvVel[self.index])
                        self.ser.write((str(self.csvVel[self.index])+'x').encode('ascii'))
                    except:
                        pass
                
                #print(self.timeQuery)
                #print(self.csvVel[self.index])
                #print(self.lineFromBoard)

                if 'Complete' in str(self.lineFromBoard) or 'Stopped' in str(self.lineFromBoard):
                    self.timeArray = self.timeArray - self.timeArray[0]
                    ## Intermediate vairable to stack both time and data arrays together
                    # print(len(self.velArray))
                    # print(len(self.refArray))
                    # print(len(self.posArray))
                    # print(len(self.timeArray))
                    
                    self.arrayForCSV = np.stack((self.velArray, self.refArray, self.posArray,self.timeArray),axis =-1)
                    
                    #self.plotStuff()
                    plt.figure(1)
                    plt.plot(self.timeArray,self.velArray, label = "Measured Vel")
                    plt.plot(self.timeArray,self.refArray, label = "Reference Vel", ls='--')
                    plt.plot(self.csvTime,self.csvVel, label = "CSV Vel", ls=':')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Rotational Velocity (RPM)')
                    plt.title('Reference Tracking, Kp = ' + self.Kpstring)
                    plt.legend()
                    plt.grid(True)
                    plt.show()
                    #plt.draw()
                    
                    plt.figure(2)
                    plt.plot(self.timeArray,self.posArray, label = "Measured Pos")
                    plt.plot(self.csvTime,self.csvPos, label = "CSV Pos", ls='--')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Rotational Position (Degrees)')
                    plt.title('Reference Tracking, Kp = ' + self.Kpstring)
                    plt.legend()
                    plt.grid(True)
                    plt.show()         
                    
                    
                    time.sleep(1)
                    self.velArray = np.array([])
                    self.posArray = np.array([])
                    self.refArray = np.array([])
                    self.timeArray = np.array([])
                    #self.lineFromBoard = ''

                    
                    #plt.savefig('Lab7_RefFollow.png')
                    
                    time.sleep(1)

                    np.savetxt('Lab7_RefFollow.csv',self.arrayForCSV,delimiter=',')
                    
                    self.transitionTo(self.S1_WAIT)

                    
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
            # updating the "Scheduled" timestamp

    
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState   