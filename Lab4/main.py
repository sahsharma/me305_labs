## @file main.py
# This is the main file that runs the Encoder and Data Generation
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 31, 2020



import pyb
from Encoder import EncoderAB
from UI_detaGen import EncoderData
#from pyb import UART


## Set Timer object
Timer = 3

## Set Prescaler object
Prescaler = 0

## Set Period object
Period = 0xFFFF

## Set ChannelA object
ChannelA = 1

## Set ChannelB object
ChannelB = 2

## Set PinA object
PinA = pyb.Pin.cpu.A6

## Set PinB object
PinB = pyb.Pin.cpu.A7

## Set CPR object
CPR = 7*4

## Set Reduction object
Reduction = 50

## Set Interval1 Object
Interval1 = (1000)*1e-6

## Build task1 object using EncoderAB class
task1 = EncoderAB(Timer, Prescaler, Period, ChannelA, PinA, ChannelB, PinB, CPR, Reduction, Interval1)



## Set UART Channel to use
MyUart = 2

## Set Number of Datapoints
DataPoints = 50

## Set Sample Rate
SampleRate = 5

## Set Interval to run Encoder Data Generation
Interval2 = (40)*1e-6

## Build task2 object using EncoderData class
task2 = EncoderData(MyUart, task1, DataPoints, SampleRate, Interval2)

## Run forever
while True:
    task1.run()
    task2.run()
