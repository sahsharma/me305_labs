## @file UI_detaGen.py
#  This script is a module that contains Encoder User Interface classes and methods to be used in other scripts.
#
#  @package UI_detaGen
#  This module contains the EncoderData interface related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 27, 2020



from pyb import UART
import utime
import array


#============================================================================#
#============================================================================#

## @brief   An EncoderData class
class EncoderData:
    ## @details   associates an encoder to send and recive data through UART
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0
    
    ## Constant defining State 1
    S1_WAIT     = 1
    
    ## Constant defining State 2
    S2_COLLECT  = 2
    
    ## Constant defining State 3
    S3_STOP     = 3
    
    
    ## @brief    Creates the EncoderData object
    def __init__(self, myuart, ENC, datapoints, sampleRate, interval):
        
        ## @brief   myuart object
        self.myuart = UART(myuart)
        
        ## @brief   ENC object
        self.ENC = ENC # Encoder object to steal data from
        
        ## @brief   sampleRate object
        self.sampleRate = int((1/sampleRate)*1e6)  #in Hz then converted to us
                
        ## @brief   datapoints object
        self.datapoints = datapoints # total number od data points to gather for a full run
        
        ## @brief   Array to hold encoder position data
        self.arrayData = 0
        
        ## @brief   Array to hold time data
        self.arrayTime = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        # The number of microseconds since bootup
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)        
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## @brief   The current time that will be updated as the code runs
        self.curr_time = 0
        
        ## @brief   Logs the number of samples that have been cycled during data collection
        self.samples = 0

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT 

        ## @brief   val object
        self.val = 0
        
        ## @brief   The statement that will be printed when data collection stops
        self.terminationStatement = 0
        
        ## @brief   Buffer value that will be added to self.arrayData
        self.bufferVal = 0
        
        ## @brief   Buffer time that will be added tot self.arrayTime
        self.bufferTime = 0
        
        ## @brief   Logs the length of the data arrays. Similar to self.samples
        self.arrayLengthCounter = 0
        
        ## @brief   The timestamp for when data collection begins
        self.begin_time = 0
        
        ## @brief   The timestamp for when to collect the next datt point/sample
        self.next_timeSample = 0

    ## @brief   runs one iteration of the task
    def run(self):
        
        ## The current time updated when the code is run
        self.curr_time = utime.ticks_us()
        # updating the current timestamp        
        
        # State 0 Init
        if (self.state == self.S0_INIT):       
        
            self.transitionTo(self.S1_WAIT)

            self.arrayData = array.array('q',[])
        
            self.arrayTime = array.array('q',[])
            
            self.myuart.write('UI Running | {:0.6f} | Type the following uppercase characters:\n\r\tG - Begin data collection\n\r\tS - Stop data collection\n\r'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))

        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0 and self.state != self.S0_INIT):

        
        
            if (self.state == self.S1_WAIT):
            
                if self.myuart.any() != 0:
                    self.val = self.myuart.readchar()
                    if self.val == 0x47:  # G
                        self.transitionTo(self.S2_COLLECT)
                        self.begin_time = utime.ticks_us()
                        self.myuart.write('Data collection beginning - please wait. {:0.6f} \r\n'.format( (utime.ticks_diff(self.curr_time, self.start_time))*1e-6 ))
                        self.next_timeSample = utime.ticks_add(self.begin_time, self.sampleRate)
                    elif self.val == 0x53:  # S
                        self.myuart.write('No data collection in progress to stop. \r\n')
                    else:
                        self.myuart.write('Invalid command. RTFD ya dingus. \r\n')
                    del(self.val)

            if (self.state == self.S2_COLLECT):
                if self.myuart.any() != 0:
                    self.val = self.myuart.readchar() 
                    if self.val == 0x53: #S
                        self.transitionTo(self.S3_STOP)
                        self.terminationStatement = 'Data Collected Stopped at {:0.6f} s. \r\n'.format( (utime.ticks_diff(self.curr_time, self.begin_time)) * 1e-6 )
                    del(self.val)
                if (self.state == self.S2_COLLECT): # Reconfirm that data collection should continue
                    ## Updates time for data collection rate        
                    self.curr_timeSamples = utime.ticks_us()
            
                    if(utime.ticks_diff(self.curr_timeSamples, self.next_timeSample) >= 0):
            
                        self.bufferVal  = self.ENC.get_position()
                        self.arrayData.append(self.bufferVal)
                        self.bufferTime = (utime.ticks_diff( utime.ticks_us(), self.begin_time ))
                        self.arrayTime.append(self.bufferTime)
                        self.arrayLengthCounter += 1
                        if self.arrayLengthCounter >= self.datapoints:
                            self.transitionTo(self.S3_STOP)
                            self.terminationStatement = 'Data Collection Completed at {:0.6f} s. \r\n'.format( (utime.ticks_diff(self.curr_time, self.begin_time)) * 1e-6 ) 
                        self.next_timeSample = utime.ticks_add(self.next_timeSample, self.sampleRate)
            
            if(self.state == self.S3_STOP):
                self.transitionTo(self.S1_WAIT)
                
                
                for n in range(0,self.arrayLengthCounter): 
                    self.myuart.write( '{:},{:}\r\n'.format(self.arrayData[n],(self.arrayTime[n])) )
                
                self.myuart.write(self.terminationStatement)
                                    
                self.arrayData = array.array('q',[])
                self.arrayTime = array.array('q',[])
                self.arrayLengthCounter = 0


                        


        self.runs += 1
        self.samples += 1
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        # updating the "Scheduled" timestamp
                    
                    
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState   
        
       
        
