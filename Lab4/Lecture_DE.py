import numpy as np
import matplotlib.pyplot as plt

alpha = 5
K = 2
x_ddot = np.empty((2,100))
x_dot = np.empty((2,100))
x = np.empty((2,100))


def solve_x_ddot(inp):
    return (-K*inp) - (alpha*(inp**3))

for init in range(0,10+1):
    
    x0 = init/10
    x[0,0] = x0
    x_dot[0,0] = 0
    
    for time in range(0,100+1):
        
        t = time/100
        x_ddot[1,time] = t
        x_dot[1,time] = t
        x[1,time] = t
        
        x_ddot[0,time] = solve_x_ddot(x[0,time])
        x
        
        
        
        

