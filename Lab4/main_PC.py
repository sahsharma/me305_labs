## @file main.py
# This is the main file that runs the Encoder and Data Generation
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 3, 2020


from UI_frontend import UI_Front
#import matplotlib as plt

## Set Interval to run UI Frontend in seconds
Interval = 1e-4

## Build task1 with UI_Front class
task1 = UI_Front(Interval)
    

## Run forever
while True:
    task1.run()
