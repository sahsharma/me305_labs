## @file UI_frontend.py
#  This script is a module that contains UI_frontend classes and methods to be used in other scripts.
#
#  @package UI_frontend
#  This module contains the User interface front end related classes and methods
#
#  
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date November 3, 2020

import serial
import numpy as np
import time
import keyboard
import matplotlib.pyplot as plt



#============================================================================#
#============================================================================#


## @brief   The UI Frontend class
class UI_Front:
    
    
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0
    
    ## Constant defining State 1
    S1_RUN     = 1
    

    ## @brief   Creates the UI_Front object
    def __init__(self, interval):

        ## @brief   Sets up serial communication to COM3
        self.ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)
        
        ## @brief   The array that will hold position data
        self.posArray = 0
        
        ## @brief   The array that will hold the time data
        self.timeArray = 0
        
        ## @brief   The line read from the board
        self.lineFromBoard = 0
        
        ## @brief User input object   
        #self.userInput = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT 
        
        ## @brief   The current time that will be updated as the code runs
        self.curr_time = 0
        
        #print('constructed')
        #print(self.start_time)
        #print(self.interval)
        #print(self.next_time)
        
    ## @brief runs one iteration fo the task
    def run(self):
    
        ## The current time updated when the code is run
        self.curr_time = time.time()    #updating the current timestamp  

        #print('C: ' + str(self.curr_time))
        #print('N: ' + str(self.next_time))

        # State 0 Init
        if (self.state == self.S0_INIT):               
        
            self.transitionTo(self.S1_RUN)
            self.posArray = np.array([])
            self.timeArray = np.array([])
            #print('Init | time: {:} | runs: {:}'.format(self.curr_time,self.runs)) 

            #keyboard.on_press_key('g',self.sendG())

        if (self.curr_time >= self.next_time):
        
            if (self.state == self.S1_RUN):
                
                #print('waiting')
                if self.ser.in_waiting > 0:
                    self.lineFromBoard = self.ser.readline().decode('ascii')
                    if (',' in str(self.lineFromBoard)) is False:    
                        print(str(self.lineFromBoard))
                elif self.ser.in_waiting == 0:
                    self.lineFromBoard = None
                if self.ser.out_waiting > 0:
                    self.ser.write()
                
                
                #keyboard.on_press_key('g',self.sendG())
                #keyboard.on_press_key('s',self.sendS())
                if keyboard.is_pressed('shift+g') == True:
                    self.ser.write('G'.encode('ascii'))
                if keyboard.is_pressed('shift+s') == True:
                    self.ser.write('S'.encode('ascii'))
                    
                if ',' in str(self.lineFromBoard):
                    ## Intermediate variable to store readline split by commas
                    self.splitLine = str(self.lineFromBoard).split(',')
                    self.posArray = np.append(self.posArray,(int(self.splitLine[0])))
                    self.timeArray = np.append(self.timeArray, (float(self.splitLine[1]))*1e-6)

                if 'Complete' in str(self.lineFromBoard) or 'Stopped' in str(self.lineFromBoard):
                    self.timeArray = self.timeArray - self.timeArray[0]
                    ## Intermediate vairable to stack both time and position data arrays together
                    self.arrayForCSV = np.stack((self.posArray,self.timeArray),axis =-1)
                    
                    #self.plotStuff()
                    plt.plot(self.timeArray,self.posArray)
                    plt.xlabel('Time (s)')
                    plt.ylabel('Position')
                    plt.title('Encoder Position')
                    plt.grid(True)
                    plt.show()
                    plt.draw()
                    
                    time.sleep(1)
                    self.posArray = np.array([])
                    self.timeArray = np.array([])

                
                    plt.savefig('Lab4_EncoderPos_Data.png')
                    np.savetxt('Lab4_EncoderPos_Data.csv',self.arrayForCSV,delimiter=',')


            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
            # updating the "Scheduled" timestamp
                    
 
    
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState   

    ## @brief   Translates keyboard press to send ascii character 'G'
    def sendG(self):
        
        self.ser.write('G'.encode('ascii'))
        
    ## @brief   Translates keyboard press to send ascii character 'S'
    def sendS(self):
        self.ser.write('S'.encode('ascii'))
        
        
#============================================================================#
#============================================================================#


    # def plotStuff(self):
    #     plt.plot(self.timeArray,self.posArray)
    #     plt.xlabel('Time (s)')
    #     plt.ylabel('Position')
    #     plt.title('Encoder Position')
    #     plt.grid(True)
        


    # def sendChar(self):
        
    #     self.inv = input('Enter capital G to begin collecting encoder data. Enter capital S to stop collecting data and save the results: ')
    #     self.ser.write(str(self.inv).encode('ascii'))
        
    #     self.myval = self.ser.readline().decode('ascii')
    #     self.splitData = self.myval.split(',')
    #     #splitData[0]
        
    #     return self.splitData
    
    
    # def readLine(self):
        
    #     if self.ser.in_waiting > 0:
    #         self.lineFromBoard = self.ser.readline().decode('ascii')
    #         self.bytesInBuffer = True
    #     elif self.ser.in_waiting == 0:
    #         self.bytesInBuffer = False
    #         self.lineFromBoard = None
    #     return [str(self.lineFromBoard), self.bytesInBuffer]
    
    
    
    
    # #for n in range(3):
    # while True:
            
    #     print(sendChar())
        
    # #ser.close()


