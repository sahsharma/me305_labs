## @file main.py
# This is the main file that calls the FSM_Elevator state machine
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 8, 2020


from FSM_Elevator import Button, Motor, FloorSensor, Elevator

# Elevator 1        
## Motor Object
Motor_E1 = Motor()

## Button Object (illuminated) for floor 1
Button1_E1 = Button('PB3','PB4')

## Button Object (illuminated) for floor 2
Button2_E1 = Button('PB5','PB6')

## Floor Sensor Object for first floor
First_E1 = FloorSensor('PB7')

## Floor Sensor Object for second floor
Second_E1 = FloorSensor('PB8')

## Elevator ID Number Object for elevator 1
Number_E1 = 1

# Elevator 2
## Motor Object
Motor_E2 = Motor()


## Button Object (illuminated) for floor 1
Button1_E2 = Button('PC3','PC4')

## Button Object (illuminated) for floor 2
Button2_E2 = Button('PC5','PC6')

## Floor Sensor Object for first floor
First_E2 = FloorSensor('PC7')

## Floor Sensor Object for second floor
Second_E2 = FloorSensor('PC8')

## Elevator ID Number Object for elevator 2
Number_E2 = 2

## Task1 Object (Elevator 1)
task1 = Elevator(0.1, Motor_E1, Button1_E1, Button2_E1, First_E1, Second_E1, Number_E1) # Will also run constructor
## Task2 Object (Elevator 2)
task2 = Elevator(0.1, Motor_E2, Button1_E2, Button2_E2, First_E2, Second_E2, Number_E2) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()

