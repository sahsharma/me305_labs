## @file FSM_Elevator.py
#  This module contains the finite-state machine for an elevator
#
#  This file serves as an example implementation of a finite-state-machine using
#  Python. The example will implement some code to control an imaginary elevator.
#
#  The user has a two buttons to select either the first floor or second floor.
#
#  There is a proximity sensor on the first and seond floors that detect whether
#  the the elevator has reached that floor.
#
#  @package FSM_Elevator
#  This module contains the finite-state machine for an elevator
#
#  Detailed doc for the FSM_Elevator module second
#
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 8, 2020
    

from random import choice
from random import choices
import time

## @brief      A finite state machine to control elevators.
class Elevator:
    ##  @details    This class implements a finite state machine to control the
    #               operation of elevators.
       
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN      = 1
    
    ## Constant defining State 2
    S2_MOVING_UP        = 2
    
    ## Constant defining State 3
    S3_STOP_FLOOR1      = 3
    
    ## Constant defining State 4
    S4_STOP_FLOOR2      = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    ## @brief      Creates an Elevator object.
    def __init__(self, interval, Motor, Button1, Button2, First, Second, Number):

        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the Button1 object
        self.Button1 = Button1

        ## A class attribute "copy" of the Button2 object
        self.Button2 = Button2
        
        ## The floor sensor object used for the first floor sensor
        self.First = First
        
        ## The floor sensor object used for the second floor sensor
        self.Second = Second
        
        ## The ID number of the elevator (one of two)
        self.Number = Number
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    

    ##  @brief      Runs one iteration of the task
    def run(self):

        
        ## The current time updated when the code is run
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            # State 0 Init
            if(self.state == self.S0_INIT):
                print('E{:}| '.format(self.Number) + str(self.runs) + ' | {:0.2f} | State 0 Init'.format(self.curr_time - self.start_time))
                # Run State 0 Code

                self.transitionTo(self.S1_MOVING_DOWN)
                self.Button1.setLight(0)
                self.Button2.setLight(0)
                self.Motor.Stop()
            
            # State 1 Down
            elif(self.state == self.S1_MOVING_DOWN):
                print('E{:}| '.format(self.Number) + str(self.runs) + ' | {:0.2f} | State 1 Down'.format(self.curr_time - self.start_time))
                # Run State 1 Code             
                
                if self.First.getFloorState():
                    self.transitionTo(self.S3_STOP_FLOOR1)
                    self.Button1.setLight(0)

                self.Motor.Down()
            
            # State 2 Up
            elif(self.state == self.S2_MOVING_UP):
                print('E{:}| '.format(self.Number) + str(self.runs) + ' | {:0.2f} | State 2 Up'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                
                if self.Second.getFloorState():
                    self.transitionTo(self.S4_STOP_FLOOR2)
                    self.Button2.setLight(0)

                self.Motor.Up()
            
            # State 3 Floor 1
            elif(self.state == self.S3_STOP_FLOOR1):
                print('E{:}| '.format(self.Number) + str(self.runs) + ' | {:0.2f} | State 3 Floor 1'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                
                if self.Button2.getButtonState():
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Button2.setLight(1)
                
                self.Motor.Stop()
                
            # State 4 Floor 2
            elif(self.state == self.S4_STOP_FLOOR2):
                print('E{:}| '.format(self.Number) + str(self.runs) + ' | {:0.2f} | State 4 Floor 2'.format(self.curr_time - self.start_time))
                # Run State 4 Code

                if self.Button1.getButtonState():
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Button1.setLight(1)
                
                self.Motor.Stop()
                    
            elif(self.state == self.S5_DO_NOTHING):
                print('E{:}| '.format(self.Number) + str(self.runs) + ' | {:0.2f} | State 5'.format(self.curr_time - self.start_time))
                
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    ## @brief      Updates the variable defining the next state to run
    def transitionTo(self, newState):
        
        self.state = newState

## @brief      A pushbutton class with optional illumination settings
## @details    This class represents a button that the can be pushed by the
#              imaginary user to select which floor to travel to. As of right now
#              this class is implemented using "pseudo-hardware". That is, we
#              are not working with real hardware I/O yet, this is all pretend.
class Button:

    
    ##  @brief              Creates a Button object
    #   @param pin_switch   A pin_switch object that the button is connected to
    #   @param pin_light    (Optional) A pin_light object that the button's light is connected to
    def __init__(self, pin_switch , pin_light = None):

        ## The pin_switch object used to read the Button state
        self.pin_switch = pin_switch
        
        ## The pin_light object used to read the illuminated button's light state
        self.pin_light = pin_light
        
        if self.pin_light is not None:
            print('Illuminated Button object created with switch attached to pin '+ str(self.pin_switch)
              + ' and light attached to pin '+ str(self.pin_light))
        else:
            print('Button object created attached to pin '+ str(self.pin))

    ## @brief      Gets a random button state.
    #  @details    Since there is no hardware attached this method 
    #              returns a randomized True or False value.
    #  @return     A boolean representing the state of the button.
    def getButtonState(self):
        
        return choice([True, False])
    
    ## @brief          Sets the illumiated button's light state.
    #  @details        Since there is no hardware attached this method sets the 
    #                  value to a varaible.
    #  @param hilo     An object that hold a high or low (0 or 1) state to turn off
    #                  or on the button's light'
    def setLight(self, hilo):
        
        ## the hilo (high-low) object used to assert an on or off status on the button's light
        self.hilo = hilo
        print('   Button Light set to {}'.format(self.hilo))

## @brief      A FloorSensor class
#  @details    This class represents a floor sensor that detects which floor
#              the elevator is present at. As of right now this class is
#              implemented using "pseudo-hardware". That is, we are not
#              working with real hardware I/O yet, this is all pretend.
class FloorSensor:
    
    ## @brief      Creates a FloorSensor object
    #  @param pin  A pin object that the sensor is connected to
    def __init__(self, pin):
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('FloorSensor object created attached to pin '+ str(self.pin))

    ## @brief      Gets the FloorSensor state.
    #  @details    Since there is no hardware attached this method
    #             returns a weighted randomized True or False value
    #             (1:10 weight). This allows virtual elevator to travel for
    #             some time before stopping in most cases.
    #  @return     A boolean representing the state of the sensor.
    def getFloorState(self):

        A = choices([True,False],[1,10])
        return A[0]

## @brief      A motor class to drive the elevator.
#  @details    This class represents a motor driver used to move the Elevator
#              up and down or hold position.
class Motor:
    
    ## @brief Creates a Motor Object
    def __init__(self):

        pass
    
    ## @brief Moves the motor to drive the Elevator up
    def Up(self):

        print('   Elevator moving UP')
    
    ## @brief Moves the motor to drive the Elevator down
    def Down(self):

        print('   Elevator moving DOWN')
    
    ## @brief Stops the Motor
    def Stop(self):

        print('   Elevator Stopped')

