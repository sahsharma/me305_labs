## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is HW 0x00 of ME 305. In this assignment, we created a python program that simulates two elevators between two floors.
#  When run, the elevators initialize and home to the first floor.
#  The button presses and sensors are generated randomly in lieu of real hardware inputs.
#  
#
#  @section sec_state_machine State Machine
#  The state machine diagram was drawn using draw.io
#  
#  @image html ME305_States.png
#
#  @section page_src Source Code Access
#  You can find the source code for the main python file here:
#  https://bitbucket.org/sahsharma/me305_labs/src/master/HW0/main.py
#  
#  Source code for the elevator state machine is here:
#  https://bitbucket.org/sahsharma/me305_labs/src/master/HW0/FSM_Elevator.py
#
#  HTML files are found here:
#  https://bitbucket.org/sahsharma/sahsharma.bitbucket.io/src/master/  
#  
#    
#  @author Sahil Sharma
#
#  @copyright License Info
#
#  @date October 6, 2020
